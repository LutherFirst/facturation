import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html'
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() pagination: any;

  pages: number[];

  depuis: number;
  jusqua: number;

  constructor() { }

  ngOnInit() {
    this.initPaginator();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.previousValue) {
      this.initPaginator();
    }

  }

  private initPaginator(): void {
    this.depuis = Math.min(Math.max(1, this.pagination.number - 4), this.pagination.totalPages - 5);
    this.jusqua = Math.max(Math.min(this.pagination.totalPages, this.pagination.number + 4), 6);

    if (this.pagination.totalPages > 5) {
      this.pages = new Array(this.jusqua - this.depuis + 1).fill(0).map((valeur, indice) => indice + this.depuis);
    } else {
      this.pages = new Array(this.pagination.totalPages).fill(0).map((valeur, indice) => indice + 1);
    }
  }
}
