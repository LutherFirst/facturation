import {HttpHandler, HttpInterceptor, HttpRequest, HttpEvent} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {Injectable} from '@angular/core';
import swal from 'sweetalert2';
import {catchError} from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(e => {
        if (e.status === 401) {

          if (this.authService.isAuthenticated()) {
            this.authService.logout();
          }
          this.router.navigate(['/login']);
        }

        if (e.status === 403) {
          swal('Accès refusé', `Hola ${this.authService.utilisateur.username} vous n'avez pas accès à cette ressource!`, 'warning');
          this.router.navigate(['/clients']);
        }
        return throwError(e);
      })
    );
  }
}
