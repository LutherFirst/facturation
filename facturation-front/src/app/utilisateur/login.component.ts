import { Component, OnInit } from '@angular/core';
import {Utilisateur} from './utilisateur';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-utilisateur',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  title = 'Login svp !!!';
  utilisateur: Utilisateur;

  constructor(private authService: AuthService, private router: Router) {
    this.utilisateur = new Utilisateur();
  }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      swal('Login', `Hello ${this.authService.utilisateur.username} vous êtes déjà authentifié!`, 'info');
      this.router.navigate(['/clients']);
    }
  }

  login(): void {
    if (this.utilisateur.username == null || this.utilisateur.password == null) {
      swal('Error Login', 'Nom d\'utilisateur ou mot de passe vide!', 'error');
      return;
    }

    this.authService.login(this.utilisateur).subscribe(response => {
        this.authService.sauvegarderUtilisateur(response.access_token);
        this.authService.sauvegarderToken(response.access_token);
        const utilisateur = this.authService.utilisateur;
        this.router.navigate(['/clients']);
        swal('Login', `Hello ${utilisateur.username}, Vous vous êtes connecté avec succès !`, 'success');
      }, err => {
        if (err.status === 400) {
          swal('Error Login', 'identifiant ou mot de passe incorrect!', 'error');
        }
      });
  }
}
