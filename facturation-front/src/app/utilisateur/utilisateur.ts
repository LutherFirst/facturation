export class Utilisateur {
  id: number;
  username: string;
  password: string;
  nom: string;
  prenom: string;
  email: string;
  roles: string;
}
