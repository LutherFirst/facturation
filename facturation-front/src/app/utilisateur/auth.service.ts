import { Injectable } from '@angular/core';
import {Utilisateur} from './utilisateur';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: Utilisateur;
  // tslint:disable-next-line:variable-name
  private _token: string;

  constructor(private http: HttpClient) { }

  public get utilisateur(): Utilisateur {
    if (this.user != null) {
      return this.user;
    } else if (this.user == null && sessionStorage.getItem('user') != null) {
      this.user = JSON.parse(sessionStorage.getItem('user')) as Utilisateur;
      return this.user;
    }
    return new Utilisateur();
  }

  public get token(): string {
    if (this._token != null) {
      return this._token;
    } else if (this._token == null && sessionStorage.getItem('token') != null) {
      this._token = sessionStorage.getItem('token');
      return this._token;
    }
    return null;
  }

  login(user: Utilisateur): Observable<any> {
    const urlEndpoint = environment.base_url + '/oauth/token';

    const credentials = btoa('angularapp' + ':' + '12345');

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: 'Basic ' + credentials
    });

    const params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', user.username);
    params.set('password', user.password);
    console.log(params.toString());
    console.log('Basic ' + credentials);
    return this.http.post<any>(urlEndpoint, params.toString(), { headers: httpHeaders });
  }

  sauvegarderUtilisateur(accessToken: string): void {
    const payload = this.retrieveDataToken(accessToken);
    this.user = new Utilisateur();
    this.user.nom = payload.nombre;
    this.user.prenom = payload.apellido;
    this.user.email = payload.email;
    this.user.username = payload.user_name;
    this.user.roles = payload.authorities;
    sessionStorage.setItem('user', JSON.stringify(this.user));
  }

  sauvegarderToken(accessToken: string): void {
    this._token = accessToken;
    sessionStorage.setItem('token', accessToken);
  }

  retrieveDataToken(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(atob(accessToken.split('.')[1]));
    }
    return null;
  }

  isAuthenticated(): boolean {
    const payload = this.retrieveDataToken(this.token);
    return payload != null && payload.user_name && payload.user_name.length > 0;
  }

  hasRole(role: string): boolean {
    return this.utilisateur.roles ? this.utilisateur.roles.includes(role) : false;
  }

  logout(): void {
    this._token = null;
    this.user = null;
    sessionStorage.clear();
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
  }
}
