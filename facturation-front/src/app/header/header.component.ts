import { Component } from '@angular/core';
import {AuthService} from '../utilisateur/auth.service';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
title = 'App Angular';
  constructor(public authService: AuthService, private router: Router) { }
  logout(): void {
    const username = this.authService.utilisateur.username;
    this.authService.logout();
    swal('Logout', `Hello ${username}, vous vous êtes déconnecté avec succès!`, 'success');
    this.router.navigate(['/login']);
  }
}
