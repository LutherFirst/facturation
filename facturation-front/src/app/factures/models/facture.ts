import {ItemFacture} from './item-facture';
import {Clients} from '../../clients/client';

export class Facture {
  id: number;
  description: string;
  observation: string;
  items: Array<ItemFacture> = [];
  client: Clients;
  total: number;
  createdAt: string;

  calculerGrandTotal(): number {
    this.total = 0;
    this.items.forEach((item: ItemFacture) => {
      this.total += item.calculerMontant();
    });
    return this.total;
  }
}
