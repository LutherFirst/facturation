import {Produit} from './produit';

export class ItemFacture {
  produit: Produit;
  quantite = 1;
  montant: number;

  public calculerMontant(): number {
    return this.quantite * this.produit.prix;
  }
}
