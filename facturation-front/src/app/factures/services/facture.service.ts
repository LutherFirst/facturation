import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Facture} from '../models/facture';
import {Produit} from '../models/produit';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FactureService {

  private urlEndPoint = environment.base_url + '/custom/factures';

  constructor(private http: HttpClient) { }

  getFacture(id: number): Observable<Facture> {
    return this.http.get<Facture>(`${this.urlEndPoint}/${id}`);
  }

  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.urlEndPoint}/${id}`);
  }

  filtrarProductos(term: string): Observable<Produit[]> {
    return this.http.get<Produit[]>(`${this.urlEndPoint}/filtrer-produits/${term}`);
  }

  create(factura: Facture): Observable<Facture> {
    return this.http.post<Facture>(this.urlEndPoint, factura);
  }
}
