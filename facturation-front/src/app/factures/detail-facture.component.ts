import { Component, OnInit } from '@angular/core';
import {Facture} from './models/facture';
import {FactureService} from './services/facture.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detail-facture',
  templateUrl: './detail-facture.component.html'
})
export class DetailFactureComponent implements OnInit {
  facture: Facture;
  title = 'Facture';
  constructor(
    private factureService: FactureService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = +params.get('id');
      this.factureService.getFacture(id).subscribe(facture => this.facture = facture);
    });
  }

}
