import { Component, OnInit } from '@angular/core';
import {Facture} from './models/facture';
import {FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FactureService} from './services/facture.service';
import {ClientService} from '../clients/client.service';
import {Observable} from 'rxjs';
import {Produit} from './models/produit';
import {flatMap, map} from 'rxjs/operators';
import {MatAutocompleteSelectedEvent} from '@angular/material';
import {ItemFacture} from './models/item-facture';
import swal from 'sweetalert2';

@Component({
  selector: 'app-factures',
  templateUrl: './factures.component.html'
})
export class FacturesComponent implements OnInit {
  title = 'Nouvelle facture';
  facture: Facture = new Facture();

  autocompleteControl = new FormControl();

  productsFiltres: Observable<Produit[]>;

  constructor(private clientService: ClientService,
              private factureService: FactureService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const clientId = +params.get('clientId');
      this.clientService.getClient(clientId).subscribe(client => this.facture.client = client);
    });

    this.productsFiltres = this.autocompleteControl.valueChanges.pipe(
        map(value => typeof value === 'string' ? value : value.nom),
        flatMap(value => value ? this._filter(value) : [])
      );
  }

  private _filter(value: string): Observable<Produit[]> {
    const filterValue = value.toLowerCase();

    return this.factureService.filtrarProductos(filterValue);
  }

  mostrarNombre(produit?: Produit): string | undefined {
    return produit ? produit.nom : undefined;
  }

  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
    const produit = event.option.value as Produit;
    console.log(produit);

    if (this.existeItem(produit.id)) {
      this.incrementaCantidad(produit.id);
    } else {
      const nouveauItem = new ItemFacture();
      nouveauItem.produit = produit;
      this.facture.items.push(nouveauItem);
    }

    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect();

  }

  actualizarCantidad(id: number, event: any): void {
    const quantite: number = event.target.value as number;

    if (quantite === 0) {
      return this.eliminarItemFactura(id);
    }

    this.facture.items = this.facture.items.map((item: ItemFacture) => {
      if (id === item.produit.id) {
        item.quantite = quantite;
      }
      return item;
    });
  }

  existeItem(id: number): boolean {
    let existe = false;
    this.facture.items.forEach((item: ItemFacture) => {
      if (id === item.produit.id) {
        existe = true;
      }
    });
    return existe;
  }

  incrementaCantidad(id: number): void {
    this.facture.items = this.facture.items.map((item: ItemFacture) => {
      if (id === item.produit.id) {
        ++item.quantite;
      }
      return item;
    });
  }

  eliminarItemFactura(id: number): void {
    this.facture.items = this.facture.items.filter((item: ItemFacture) => id !== item.produit.id);
  }

  create(factureForm): void {
    console.log(this.facture);
    if (this.facture.items.length === 0) {
      this.autocompleteControl.setErrors({ invalid: true });
    }

    if (factureForm.form.valid && this.facture.items.length > 0) {
      this.factureService.create(this.facture).subscribe(facture => {
        swal(this.title, `Facture ${facture.description} creer avec success!`, 'success');
        this.router.navigate(['/clients']);
      });
    }
  }
}
