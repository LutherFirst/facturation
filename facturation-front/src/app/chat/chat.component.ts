import { Component, OnInit } from '@angular/core';
import { Client } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {Message} from './models/message';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  private client: Client;

  connecte = false;

  message: Message = new Message();
  messages: Message[] = [];

  escribiendo: string;
  clientId: string;

  constructor() {
    this.clientId = 'id-' + new Date().getTime() + '-' + Math.random().toString(36).substr(2);
  }

  ngOnInit() {
    this.client = new Client();
    this.client.webSocketFactory = () => {
      return new SockJS('http://localhost:8086/chat-websocket');
    };

    this.client.onConnect = (frame) => {
      console.log('Connecté: ' + this.client.connected + ' : ' + frame);
      this.connecte = true;

      this.client.subscribe('/chat/message', e => {
        const message: Message = JSON.parse(e.body) as Message;
        message.createdAt = new Date(message.createdAt);

        if (!this.message.color && message.type === 'NOUVEL_USER' &&
          this.message.username === message.username) {
          this.message.color = message.color;
        }
        this.messages.push(message);
      });

      this.client.subscribe('/chat/escribiendo', e => {
        this.escribiendo = e.body;
        setTimeout(() => this.escribiendo = '', 3000);
      });
      console.log(this.clientId);
      this.client.subscribe('/chat/historique/' + this.clientId, e => {
        const historial = JSON.parse(e.body) as Message[];
        this.messages = historial.map(m => {
          m.createdAt = new Date(m.createdAt);
          return m;
        }).reverse();
      });

      this.client.publish({ destination: '/app/historique', body: this.clientId });

      this.message.type = 'NOUVEL_USER';
      this.client.publish({ destination: '/app/message', body: JSON.stringify(this.message) });
    };

    this.client.onDisconnect = (frame) => {
      console.log('Deconnecter: ' + !this.client.connected + ' : ' + frame);
      this.connecte = false;
      this.message = new Message();
      this.messages = [];
    };
  }

  conectar(): void {
    this.client.activate();
  }

  desconectar(): void {
    this.client.deactivate();
  }

  enviarMensaje(): void {
    this.message.type = 'Message';
    this.client.publish({ destination: '/app/message', body: JSON.stringify(this.message) });
    this.message.texto = '';
  }

  escribiendoEvento(): void {
    this.client.publish({ destination: '/app/escribiendo', body: this.message.username });
  }

}
