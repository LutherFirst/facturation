export class Message {
  texto = '';
  createdAt: Date;
  username: string;
  type: string;
  color: string;
}
