import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directive',
  templateUrl: './directive.component.html'
})
export class DirectiveComponent implements OnInit {
  coursList = ['TypeScript', 'JavaScript', 'Java SE', 'C#', 'PHP'];
  habilitation = true;
  constructor() { }

  ngOnInit() {
  }

  setHabilitation(): void {
    this.habilitation = (this.habilitation !== true);
  }

}
