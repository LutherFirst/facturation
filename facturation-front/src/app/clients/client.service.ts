import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {Region} from './region';
import {map, catchError, tap} from 'rxjs/operators';
import {Clients} from './client';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  public baseurl = environment.base_url;

  private urlEndPoint = environment.base_url + '/custom/clients';

  constructor(private http: HttpClient, private router: Router) { }

  getRegions(): Observable<Region[]> {
    return this.http.get<Region[]>(this.urlEndPoint + '/regions');
  }

  getClients(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page).pipe(
      tap((response: any) => {
        // (response.content as Clients[]).forEach(client => console.log(client.prenom));
      }),

      map((response: any) => {
        (response.content as Clients[]).map(client => {
          client.nom = client.nom.toUpperCase();
          return client;
        });
        return response;
      }),

      tap(response => {
        // (response.content as Clients[]).forEach(client => console.log(client.nom));
      })
    );
  }

  create(client: Clients): Observable<Clients> {
    return this.http.post(this.urlEndPoint, client).pipe(
      map((response: any) => response.client as Clients),
      catchError(e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.message) {
          console.error(e.error.message);
        }
        return throwError(e);
      }));
  }

  getClient(id): Observable<Clients> {
    return this.http.get<Clients>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e => {
        if (e.status !== 401 && e.error.message) {
          this.router.navigate(['/clients']);
          console.error(e.error.message);
        }
        return throwError(e);
      }));
  }

  update(client: Clients): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${client.id}`, client).pipe(
      catchError(e => {
        if (e.status === 400) {
          return throwError(e);
        }
        if (e.error.message) {
          console.error(e.error.message);
        }
        return throwError(e);
      }));
  }

  delete(id: number): Observable<Clients> {
    return this.http.delete<Clients>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e => {
        if (e.error.message) {
          console.error(e.error.message);
        }
        return throwError(e);
      }));
  }

  soumettrePhoto(archive: File, id): Observable<HttpEvent<{}>> {
    const formData = new FormData();
    formData.append('archive', archive);
    formData.append('id', id);

    const req = new HttpRequest('POST', `${this.urlEndPoint}/upload`, formData, {reportProgress: true});

    return this.http.request(req);
  }
}
