import { Component, OnInit } from '@angular/core';
import { Clients } from './client';
import { ClientService } from './client.service';
import { ModalService } from './details/modal.service';
import swal from 'sweetalert2';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../utilisateur/auth.service';


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html'
})
export class ClientsComponent implements OnInit {

  clients: Clients[];
  pagination: any;
  clientSelectionne: Clients;

  constructor(
    public clientService: ClientService,
    private modalService: ModalService,
    public authService: AuthService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');
      if (!page) {
        page = 0;
      }

      this.clientService.getClients(page).pipe(tap(response => {
        // (response.content as Clients[]).forEach(client => console.log(client.nom));
      })).subscribe(response => {
        this.clients = response.content as Clients[];
        this.pagination = response;
      });
    });

    this.modalService.notificarUpload.subscribe(client => {
      this.clients = this.clients.map(clientOriginal => {
        if (client.id === clientOriginal.id) {
          clientOriginal.photo = client.photo;
        }
        return clientOriginal;
      });
    });
  }

  delete(client: Clients): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar al cliente ${client.nom} ${client.prenom}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.clientService.delete(client.id).subscribe(
          () => {
            this.clients = this.clients.filter(cli => cli !== client);
            swal(
              'Cliente Eliminado!',
              `Cliente ${client.nom} eliminado con éxito.`,
              'success'
            );
          }
        );

      }
    });
  }

  abrirModal(client: Clients) {
    this.clientSelectionne = client;
    this.modalService.ouvrirModal();
  }


}
