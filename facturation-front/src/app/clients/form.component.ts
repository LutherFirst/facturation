import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import {Region} from './region';
import {Clients} from './client';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService} from './client.service';

@Component({
  selector: 'app-details',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  public client: Clients = new Clients();
  regions: Region[];
  title = 'Inscrire un Client';

  errors: string[];

  constructor(
    private clientService: ClientService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = +params.get('id');
      if (id) {
        this.clientService.getClient(id).subscribe((client) => this.client = client);
      }
    });

    this.clientService.getRegions().subscribe(regions => this.regions = regions);
  }

  create(): void {
    console.log(this.client);
    this.clientService.create(this.client)
      .subscribe(
        client => {
          this.router.navigate(['/clients']);
          swal('Nouveau client', `Le client ${client.nom} a été créé avec succès`, 'success');
        },
        err => {
          this.errors = err.error.errors as string[];
          console.error('Code d\'erreur du backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  }

  update(): void {
    console.log(this.client);
    this.client.factures = null;
    this.clientService.update(this.client)
      .subscribe(
        json => {
          this.router.navigate(['/clients']);
          swal('Client mis à jour', `${json.message}: ${json.client.nom}`, 'success');
        },
        err => {
          this.errors = err.error.errors as string[];
          console.error('Code d\'erreur du backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  }

  comparerRegion(o1: Region, o2: Region): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }
}
