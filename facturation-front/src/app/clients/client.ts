import { Region } from './region';
import {Facture} from '../factures/models/facture';

export class Clients {
  id: number;
  nom: string;
  prenom: string;
  createdAt: string;
  email: string;
  photo: string;
  region: Region;
  factures: Array<Facture> = [];
}
