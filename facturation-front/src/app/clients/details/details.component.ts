import {Component, Input, OnInit} from '@angular/core';
import {Clients} from '../client';
import {ClientService} from '../client.service';
import {AuthService} from '../../utilisateur/auth.service';
import {FactureService} from '../../factures/services/facture.service';
import {ModalService} from './modal.service';
import swal from 'sweetalert2';
import {HttpEventType} from '@angular/common/http';
import {Facture} from '../../factures/models/facture';

@Component({
  selector: 'app-detail-client',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  @Input() client: Clients;

  title = 'Detalle del cliente';
  public photoSelectionne: File;
  progression = 0;

  constructor(public clientService: ClientService,
              private factureService: FactureService,
              public authService: AuthService,
              public modalService: ModalService) { }

  ngOnInit() { }

  seleccionarFoto(event) {
    this.photoSelectionne = event.target.files[0];
    this.progression = 0;
    if (this.photoSelectionne.type.indexOf('image') < 0) {
      swal('Error seleccionar imagen: ', 'El archivo debe ser del tipo imagen', 'error');
      this.photoSelectionne = null;
    }
  }

  subirFoto() {
    if (!this.photoSelectionne) {
      swal('Error Upload: ', 'Debe seleccionar una foto', 'error');
    } else {
      this.clientService.soumettrePhoto(this.photoSelectionne, this.client.id)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progression = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            const response: any = event.body;
            this.client = response.client as Clients;

            this.modalService.notificarUpload.emit(this.client);
            swal('La foto se ha subido completamente!', response.message, 'success');
          }
        });
    }
  }

  cerrarModal() {
    this.modalService.fermerModal();
    this.photoSelectionne = null;
    this.progression = 0;
  }

  delete(facture: Facture): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar la factura ${facture.description}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.factureService.delete(facture.id).subscribe(
          () => {
            this.client.factures = this.client.factures.filter(f => f !== facture);
            swal(
              'Factura Eliminada!',
              `Factura ${facture.description} eliminada con éxito.`,
              'success'
            );
          }
        );

      }
    });
  }
}
