import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  modal = false;
  private notifieUpload = new EventEmitter<any>();

  constructor() { }

  get notificarUpload(): EventEmitter<any> {
    return this.notifieUpload;
  }

  ouvrirModal() {
    this.modal = true;
  }

  fermerModal() {
    this.modal = false;
  }
}
