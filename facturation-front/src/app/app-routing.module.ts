import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DirectiveComponent} from './directive/directive.component';
import {ClientsComponent} from './clients/clients.component';
import {DetailsComponent} from './clients/details/details.component';
import {FacturesComponent} from './factures/factures.component';
import {LoginComponent} from './utilisateur/login.component';
import {FormComponent} from './clients/form.component';
import {RoleGuard} from './utilisateur/guards/role.guard';
import {AuthGuard} from './utilisateur/guards/auth.guard';
import {ChatComponent} from './chat/chat.component';


const routes: Routes = [
  { path: '', redirectTo: '/clients', pathMatch: 'full' },
  { path: 'directives', component: DirectiveComponent },
  { path: 'clients', component: ClientsComponent },
  { path: 'chats', component: ChatComponent },
  { path: 'clients/page/:page', component: ClientsComponent },
  { path: 'clients/form', component: FormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } },
  { path: 'clients/form/:id', component: FormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } },
  { path: 'login', component: LoginComponent },
  { path: 'factures/:id', component: DetailsComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_USER' } },
  { path: 'factures/form/:clientId', component: FacturesComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
