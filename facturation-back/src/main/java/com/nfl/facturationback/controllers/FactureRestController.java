package com.nfl.facturationback.controllers;

import com.nfl.facturationback.models.entity.Facture;
import com.nfl.facturationback.models.entity.Produit;
import com.nfl.facturationback.models.services.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = { "http://localhost:4200",  "http://localhost:4300"})
@RestController
public class FactureRestController {

	@Autowired
	private IClientService clientService;

	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping("/factures/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Facture show(@PathVariable Long id) {
		return clientService.findFactureById(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@DeleteMapping("/factures/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		clientService.deleteFactureById(id);
	}
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/factures/filtrer-produits/{term}")
	@ResponseStatus(HttpStatus.OK)
	public List<Produit> filtrarProductos(@PathVariable String term){
		return clientService.findProduitByNom(term);
	}
	
	@Secured({"ROLE_ADMIN"})
	@PostMapping("/factures")
	@ResponseStatus(HttpStatus.CREATED)
	public Facture crear(@RequestBody Facture facture) {
		return clientService.saveFacture(facture);
	}

}
