package com.nfl.facturationback.controllers;

import com.nfl.facturationback.models.entity.Client;
import com.nfl.facturationback.models.entity.Region;
import com.nfl.facturationback.models.services.IClientService;
import com.nfl.facturationback.models.services.IUploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin(origins = { "http://localhost:4200", "http://localhost:4300" })
@RestController
@RequestMapping("/custom")
public class ClientRestController {

	@Autowired
	private IClientService clientService;

	@Autowired
	private IUploadFileService uploadService;

	private final Logger log = LoggerFactory.getLogger(ClientRestController.class);

	@GetMapping("/clients")
	public List<Client> index() {
		log.info("Liste de tous les clients non paginee");
		return clientService.findAll();
	}

	@GetMapping("/clients/page/{page}")
	public Page<Client> index(@PathVariable Integer page) {
		log.info("Affichage de clients paginee !!!");
		Pageable pageable = PageRequest.of(page, 4);
		return clientService.findAll(pageable);
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping("/clients/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		log.info("afficher un client");
		Client client = null;
		Map<String, Object> response = new HashMap<>();

		try {
			client = clientService.findById(id);
		} catch(DataAccessException e) {
			response.put("message", "Error al realizar la consulta en la base de datos");
			response.put("error", Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if(client == null) {
			response.put("message", "El cliente ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(client, HttpStatus.OK);
	}

	@Secured("ROLE_ADMIN")
	@PostMapping("/clients")
	public ResponseEntity<?> create(@Valid @RequestBody Client client, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		log.info(client.toString());

		if (exposeBadRequestError(result, response)) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		Client clientNew;
		try {
			clientNew = clientService.save(client);
		} catch(DataAccessException e) {
			response.put("message", "Error al realizar el insert en la base de datos");
			response.put("error", Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "El cliente ha sido creado con éxito!");
		response.put("client", clientNew);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@Secured("ROLE_ADMIN")
	@PutMapping("/clients/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Client client, BindingResult result, @PathVariable Long id) {
		Client clientActual = clientService.findById(id);
		Map<String, Object> response = new HashMap<>();

		if (exposeBadRequestError(result, response)) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

		if (clientActual == null) {
			response.put("message", "Error: no se pudo editar, el cliente ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		}
		Client clientUpdated = null;
		try {
			/*clientActual.setPrenom(client.getPrenom());
			clientActual.setNom(client.getNom());
			clientActual.setEmail(client.getEmail());
			clientActual.setCreatedAt(client.getCreatedAt());
			clientActual.setRegion(client.getRegion());*/
			clientUpdated = clientService.save(Client.builder()
					.prenom(client.getPrenom()).nom(client.getNom())
					.email(client.getEmail()).createdAt(client.getCreatedAt())
					.region(client.getRegion()).build());
		} catch (DataAccessException e) {
			response.put("message", "Error al actualizar el cliente en la base de datos");
			response.put("error", Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("message", "El cliente ha sido actualizado con éxito!");
		response.put("client", clientUpdated);

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	private boolean exposeBadRequestError(BindingResult result, Map<String, Object> response) {
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			response.put("errors", errors);
			return true;
		}
		return false;
	}

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/clients/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		Map<String, Object> response = new HashMap<>();

		try {
			Client client = clientService.findById(id);
			String nomPhotoAnterieur = client.getPhoto();

			uploadService.supprimer(nomPhotoAnterieur);
			clientService.delete(id);
		} catch (DataAccessException e) {
			response.put("message", "Error al eliminar el cliente de la base de datos");
			response.put("error", Objects.requireNonNull(e.getMessage()).concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("message", "El cliente eliminado con éxito!");

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@PostMapping("/clients/upload")
	public ResponseEntity<?> upload(@RequestParam("archive") MultipartFile archive, @RequestParam("id") Long id) {

		Map<String, Object> response = new HashMap<>();
		Client client = clientService.findById(id);

		if(!archive.isEmpty()) {
			String nomArchive;
			try {
				nomArchive = uploadService.copier(archive);
			} catch (IOException e) {
				response.put("message", "Error al subir la imagen del cliente");
				response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
				return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			String nomPhotoAnterieur = client.getPhoto();
			uploadService.supprimer(nomPhotoAnterieur);
			client.setPhoto(nomArchive);
			clientService.save(client);

			response.put("client", client);
			response.put("message", "Has subido correctamente la imagen: " + nomArchive);
		}

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@GetMapping("/uploads/img/{nomPhoto:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String nomPhoto){

		Resource resource = null;

		try {
			resource = uploadService.charger(nomPhoto);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		HttpHeaders entete = new HttpHeaders();
		assert resource != null;
		entete.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"");

		return new ResponseEntity<>(resource, entete, HttpStatus.OK);
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/clients/regions")
	public List<Region> listarRegiones(){
		return clientService.findAllRegions();
	}
}
