package com.nfl.facturationback.controllers;

import com.nfl.facturationback.models.entity.Message;
import com.nfl.facturationback.models.services.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Date;
import java.util.Random;

@Controller
public class ChatController {
    private String[] colors = {"red", "green", "blue", "magenta", "purple", "orange"};

    @Autowired
    private ChatService chatService;

    @Autowired
    private SimpMessagingTemplate webSocket;

    @MessageMapping("/message")
    @SendTo("/chat/message")
    public Message recibeMensaje(Message message) {
        message.setCreatedAt(new Date().getTime());

        if(message.getType().equals("NOUVEL_USER")) {
            message.setColor(colors[new Random().nextInt(colors.length)]);
            message.setTexto("nouvel utilisateur");
        } else {
            chatService.enregistrer(message);
        }

        return message;
    }

    @MessageMapping("/escribiendo")
    @SendTo("/chat/escribiendo")
    public String estaEscribiendo(String username) {
        return username.concat(" está escribiendo ...");
    }

    @MessageMapping("/historique")
    public void historial(String clienteId){
        webSocket.convertAndSend("/chat/historique/" + clienteId, chatService.obtenirUltimos10Messages());
    }
}
