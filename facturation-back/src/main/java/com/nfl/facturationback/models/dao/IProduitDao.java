package com.nfl.facturationback.models.dao;

import com.nfl.facturationback.models.entity.Produit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
public interface IProduitDao extends CrudRepository<Produit, Long> {

	@Query("select p from Produit p where p.nom like %?1%")
	public List<Produit> findByNom(String term);
	
	public List<Produit> findByNomContainingIgnoreCase(String term);
	
	public List<Produit> findByNomStartingWithIgnoreCase(String term);
}
