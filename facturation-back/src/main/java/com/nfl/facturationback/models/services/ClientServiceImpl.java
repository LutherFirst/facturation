package com.nfl.facturationback.models.services;
import com.nfl.facturationback.models.dao.IClientDao;
import com.nfl.facturationback.models.dao.IFactureDao;
import com.nfl.facturationback.models.dao.IProduitDao;
import com.nfl.facturationback.models.entity.Client;
import com.nfl.facturationback.models.entity.Facture;
import com.nfl.facturationback.models.entity.Produit;
import com.nfl.facturationback.models.entity.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements IClientService {

	@Autowired
	private IClientDao clientDao;

	@Autowired
	private IFactureDao factureDao;

	@Autowired
	private IProduitDao produitDao;

	@Override
	@Transactional(readOnly = true)
	public List<Client> findAll() {
		return clientDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Client> findAll(Pageable pageable) {
		return clientDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Client findById(Long id) {
		return clientDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Client save(Client client) {
		return clientDao.save(client);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		clientDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Region> findAllRegions() {
		return clientDao.findAllRegions();
	}

	@Override
	@Transactional(readOnly = true)
	public Facture findFactureById(Long id) {
		return factureDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Facture saveFacture(Facture facture) {
		return factureDao.save(facture);
	}

	@Override
	@Transactional
	public void deleteFactureById(Long id) {
		factureDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Produit> findProduitByNom(String term) {
		return produitDao.findByNomContainingIgnoreCase(term);
	}

	@Override
	@Transactional
	public List<Client> getAllClients(Integer pageNo, Integer pageSize, String sortBy) {

		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<Client> pagedResult = clientDao.findAll(paging);

		if(pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<Client>();
		}
	}

}
