package com.nfl.facturationback.models.utils;

import com.google.zxing.client.j2se.MatrixToImageConfig;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class QrCodeUtil {
    public static MatrixToImageConfig getMatrixConfig() {
        return new MatrixToImageConfig(Colors.ORANGE.getArgb(),
                Colors.WHITE.getArgb());
    }

    public static BufferedImage getOverly(String imgPath, double percent) {
        BufferedImage image = null;
        try {
            image = resize(imgPath, percent);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return image;
    }

    public enum Colors {

        BLUE(0xFF40BAD0),
        RED(0xFFE91C43),
        PURPLE(0xFF8A4F9E),
        ORANGE(0xFFF4B13D),
        WHITE(0xFFFFFFFF),
        BLACK(0xFF000000);

        private final int argb;

        Colors(final int argb){
            this.argb = argb;
        }

        public int getArgb(){
            return argb;
        }
    }

    public static BufferedImage resize(String inputImagePath,
                                       int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
        return outputImage;
    }

    public static BufferedImage resize(String inputImagePath, double percent) throws IOException {
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
        int scaledWidth = (int) (inputImage.getWidth() * percent);
        int scaledHeight = (int) (inputImage.getHeight() * percent);
        return resize(inputImagePath, scaledWidth, scaledHeight);
    }

    public static BufferedImage combineImageWithOverly(BufferedImage qrImage, BufferedImage overly) throws IOException {
        // Calculate the delta height and width between QR code and logo
        int deltaHeight = qrImage.getHeight() - overly.getHeight();
        int deltaWidth = qrImage.getWidth() - overly.getWidth();

        // Initialize combined image
        BufferedImage combined = new BufferedImage(qrImage.getHeight(), qrImage.getWidth(),
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) combined.getGraphics();
        // Write QR code to new image at position 0/0
        g.drawImage(qrImage, 0, 0, null);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

        g.drawImage(overly, (int)Math.round((double) deltaWidth / 2), (int) Math.round((double)deltaHeight / 2), null);

        // Write combined image as PNG to OutputStream
        ImageIO.write(combined, "png", new ByteArrayOutputStream());

        return combined;
    }
}
