package com.nfl.facturationback.models.dao;

import com.nfl.facturationback.models.entity.Utilisateur;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IUtilisateurDao extends CrudRepository<Utilisateur, Long>{
	
	public Utilisateur findByUsername(String username);
	
	@Query("select u from Utilisateur u where u.username=?1")
	public Utilisateur findByUsername2(String username);

}
