package com.nfl.facturationback.models.dao;

import com.nfl.facturationback.models.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatDao extends JpaRepository<Message, Long> {
    public List<Message> findFirst10ByOrderByCreatedAtDesc();
}
