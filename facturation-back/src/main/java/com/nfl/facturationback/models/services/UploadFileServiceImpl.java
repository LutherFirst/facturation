package com.nfl.facturationback.models.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

@Service
public class UploadFileServiceImpl implements IUploadFileService{
	
	private final Logger log = LoggerFactory.getLogger(UploadFileServiceImpl.class);
	
	private final static String DIRECTORIO_UPLOAD = "uploads";

	@Override
	public Resource charger(String nomPhoto) throws MalformedURLException {
		
		Path routeArchive = getPath(nomPhoto);
		log.info(routeArchive.toString());
		
		Resource urlResource = new UrlResource(routeArchive.toUri());
		
		if(!urlResource.exists() && !urlResource.isReadable()) {
			routeArchive = Paths.get("src/main/resources/static/images").resolve("no-usuario.png").toAbsolutePath();

			urlResource = new UrlResource(routeArchive.toUri());
			
			log.error("Error no se pudo cargar la imagen: " + nomPhoto);
			
		}
		return urlResource;
	}

	@Override
	public String copier(MultipartFile archive) throws IOException {
		
		String nomArchive = UUID.randomUUID().toString() + "_" +  Objects.requireNonNull(archive.getOriginalFilename()).replace(" ", "");
		
		Path routeArchive = getPath(nomArchive);
		log.info(routeArchive.toString());
		
		Files.copy(archive.getInputStream(), routeArchive);
		
		return nomArchive;
	}

	@Override
	public boolean supprimer(String nomPhoto) {
		
		if(nomPhoto != null && nomPhoto.length() > 0) {
			Path routePhotoAnterieur = Paths.get("uploads").resolve(nomPhoto).toAbsolutePath();
			File archivePhotoAnterieur = routePhotoAnterieur.toFile();
			if(archivePhotoAnterieur.exists() && archivePhotoAnterieur.canRead()) {
				archivePhotoAnterieur.delete();
				return true;
			}
		}
		
		return false;
	}

	@Override
	public Path getPath(String nomPhoto) {
		return Paths.get(DIRECTORIO_UPLOAD).resolve(nomPhoto).toAbsolutePath();
	}

}
