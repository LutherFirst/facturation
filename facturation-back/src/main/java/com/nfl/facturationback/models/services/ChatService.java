package com.nfl.facturationback.models.services;

import com.nfl.facturationback.models.entity.Message;

import java.util.List;

public interface ChatService {
    public List<Message> obtenirUltimos10Messages();
    public Message enregistrer(Message message);
}
