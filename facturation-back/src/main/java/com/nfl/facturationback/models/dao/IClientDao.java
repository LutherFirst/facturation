package com.nfl.facturationback.models.dao;

import com.nfl.facturationback.models.entity.Client;
import com.nfl.facturationback.models.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IClientDao extends JpaRepository<Client, Long>{

	@Query("from Region")
	public List<Region> findAllRegions();
}
