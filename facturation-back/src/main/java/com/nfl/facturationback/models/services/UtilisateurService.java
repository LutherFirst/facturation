package com.nfl.facturationback.models.services;

import com.nfl.facturationback.models.dao.IUtilisateurDao;
import com.nfl.facturationback.models.entity.Utilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UtilisateurService implements IUtilisateurService, UserDetailsService{
	
	private Logger logger = LoggerFactory.getLogger(UtilisateurService.class);

	@Autowired
	private IUtilisateurDao utilisateurDao;
	
	@Override
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Utilisateur utilisateur = utilisateurDao.findByUsername(username);
		
		if(utilisateur == null) {
			logger.error("Error en el login: no existe el usuario '"+username+"' en el sistema!");
			throw new UsernameNotFoundException("Error en el login: no existe el usuario '"+username+"' en el sistema!");
		}
		
		List<GrantedAuthority> authorities = utilisateur.getRoles()
				.stream()
				.map(role -> new SimpleGrantedAuthority(role.getNom()))
				.peek(authority -> logger.info("Role: " + authority.getAuthority()))
				.collect(Collectors.toList());
		
		return new User(utilisateur.getUsername(), utilisateur.getPassword(),
				utilisateur.getEnabled(), true, true, true, authorities);
	}

	@Override
	@Transactional(readOnly=true)
	public Utilisateur findByUsername(String username) {
		return utilisateurDao.findByUsername(username);
	}

}
