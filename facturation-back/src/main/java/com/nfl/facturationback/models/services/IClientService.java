package com.nfl.facturationback.models.services;


import com.nfl.facturationback.models.entity.Client;
import com.nfl.facturationback.models.entity.Facture;
import com.nfl.facturationback.models.entity.Produit;
import com.nfl.facturationback.models.entity.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IClientService {

	public List<Client> findAll();

	public Page<Client> findAll(Pageable pageable);

	public Client findById(Long id);

	public Client save(Client client);

	public void delete(Long id);

	public List<Region> findAllRegions();

	public Facture findFactureById(Long id);

	public Facture saveFacture(Facture facture);

	public void deleteFactureById(Long id);

	public List<Produit> findProduitByNom(String term);

	public List<Client> getAllClients(Integer pageNo, Integer pageSize, String sortBy);

}
