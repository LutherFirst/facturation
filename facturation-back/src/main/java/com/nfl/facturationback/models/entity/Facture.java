package com.nfl.facturationback.models.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "factures")
@Data
public class Facture implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String description;

	private String observation;

	@Column(name = "created_at")
	@Temporal(TemporalType.DATE)
	private Date createdAt;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@ManyToOne(fetch = FetchType.LAZY)
	private Client client;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "facture_id")
	private List<ItemFacture> items;

	public Facture() {
		items = new ArrayList<>();
	}

	@PrePersist
	public void prePersist() {
		this.createdAt = new Date();
	}

	public Double getTotal() {
		Double total = 0.00;
		for (ItemFacture item : items) {
			total += item.getMontant();
		}
		return total;
	}

	private static final long serialVersionUID = 1L;
}
