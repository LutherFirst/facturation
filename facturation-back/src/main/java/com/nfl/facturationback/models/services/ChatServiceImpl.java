package com.nfl.facturationback.models.services;

import com.nfl.facturationback.models.dao.ChatDao;
import com.nfl.facturationback.models.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatServiceImpl implements ChatService {
    @Autowired
    private ChatDao chatDao;

    @Override
    public List<Message> obtenirUltimos10Messages() {
        return chatDao.findFirst10ByOrderByCreatedAtDesc();
    }

    @Override
    public Message enregistrer(Message message) {
        return chatDao.save(message);
    }
}
