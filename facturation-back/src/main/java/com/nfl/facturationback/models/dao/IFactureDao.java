package com.nfl.facturationback.models.dao;

import com.nfl.facturationback.models.entity.Facture;
import org.springframework.data.repository.CrudRepository;

public interface IFactureDao extends CrudRepository<Facture, Long>{

}
