package com.nfl.facturationback.models.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "factures_items")
@Data
public class ItemFacture implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer quantite;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "produit_id")
	private Produit produit;

	public Double getMontant() {
		return quantite.doubleValue() * produit.getPrix();
	}

	private static final long serialVersionUID = 1L;
}
