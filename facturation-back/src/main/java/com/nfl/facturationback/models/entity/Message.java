package com.nfl.facturationback.models.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table( name = "messages")
@Data
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String texto;
    private Long createdAt;
    private String username;
    private String type;
    private String color;
}
