package com.nfl.facturationback.models.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

public interface IUploadFileService {

	public Resource charger(String nomPhoto) throws MalformedURLException;
	public String copier(MultipartFile archive) throws IOException;
	public boolean supprimer(String nomPhoto);
	public Path getPath(String nomPhoto);
}
