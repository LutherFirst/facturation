package com.nfl.facturationback.models.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="roles")
@Data
public class Role implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(unique=true, length=20)
	private String nom;
	private static final long serialVersionUID = 1L;
}
