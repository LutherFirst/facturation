package com.nfl.facturationback.models.services;

import com.nfl.facturationback.models.entity.Utilisateur;

public interface IUtilisateurService {

	public Utilisateur findByUsername(String username);
}
