package com.nfl.facturationback.auth;

import com.nfl.facturationback.models.entity.Utilisateur;
import com.nfl.facturationback.models.services.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InfoAdicionalToken implements TokenEnhancer {
	
	@Autowired
	private IUtilisateurService utilisateurService;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		
		Utilisateur utilisateur = utilisateurService.findByUsername(authentication.getName());
		Map<String, Object> info = new HashMap<>();
		info.put("info_additionnel", "Hello comment tu vas!: ".concat(authentication.getName()));
		
		info.put("nom", utilisateur.getNom());
		info.put("prenom", utilisateur.getPrenom());
		info.put("email", utilisateur.getEmail());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		
		return accessToken;
	}

}
